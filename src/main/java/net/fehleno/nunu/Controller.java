package net.fehleno.nunu;


import java.util.HashMap;
import java.util.Map;

import com.github.strikerx3.jxinput.XInputComponents;


public class Controller
{
	public static final float SQUEEZE_THRESHHOLD = 0.9F;
	private final Map<Button, State> states = new HashMap<>();
	
	public void update(XInputComponents xButtons)
	{
		for (Button button : Button.values())
		{
			update(xButtons, button);
		}
	}
	
	private void update(XInputComponents xButtons, Button button)
	{
		State state = getStateOf(button);
		if (state == State.PRESSED)
		{
			state = State.DOWN;
		}
		if (state == State.RELEASED)
		{
			state = State.UP;
		}
		boolean held = button.check(xButtons);
		if (held)
		{
			if (state == State.UP)
			{
				state = State.PRESSED;
			}
		}
		else
		{
			if (state == State.DOWN)
			{
				state = State.RELEASED;
			}
		}
		states.put(button, state);
	}
	
	public State getStateOf(Button button)
	{
		return states.getOrDefault(button, State.UP);
	}
	
	public boolean isHeld(Button button)
	{
		return getStateOf(button).isHeld();
	}
	
	public boolean isJustPressed(Button button)
	{
		return getStateOf(button) == State.PRESSED;
	}
	
	public boolean isJustReleased(Button button)
	{
		return getStateOf(button) == State.RELEASED;
	}
	
	public enum State
	{
		UP(false),
		RELEASED(false),
		DOWN(true),
		PRESSED(true);
		
		private boolean held;
		
		private State(boolean held)
		{
			this.held = held;
		}
		
		public boolean isHeld()
		{
			return held;
		}
	}
	
	public enum Button
	{
		A((components) -> components.getButtons().a),
		B((components) -> components.getButtons().b),
		X((components) -> components.getButtons().x),
		Y((components) -> components.getButtons().y),
		RT((components) -> components.getAxes().rt > SQUEEZE_THRESHHOLD),
		LT((components) -> components.getAxes().lt > SQUEEZE_THRESHHOLD),
		RB((components) -> components.getButtons().rShoulder),
		LB((components) -> components.getButtons().lShoulder),
		DL((components) -> components.getButtons().left),
		GD((components) -> components.getButtons().back),
		ST((components) -> components.getButtons().start),
		UP((components) -> components.getButtons().up),
		DW((components) -> components.getButtons().down),
		RG((components) -> components.getButtons().right);
		
		private final DeviceButtonChecker checker;
		
		private Button(DeviceButtonChecker checker)
		{
			this.checker = checker;
		}
		
		public boolean check(XInputComponents xButtons)
		{
			return checker.check(xButtons);
		}
	}
	
	private interface DeviceButtonChecker
	{
		public boolean check(XInputComponents device);
	}
}

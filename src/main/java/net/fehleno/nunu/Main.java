package net.fehleno.nunu;


import java.awt.MouseInfo;
import java.awt.Point;

import com.github.strikerx3.jxinput.XInputAxes;
import com.github.strikerx3.jxinput.XInputComponents;
import com.github.strikerx3.jxinput.XInputDevice14;
import com.github.strikerx3.jxinput.exceptions.XInputNotLoadedException;

import net.fehleno.nunu.Controller.Button;


public class Main
{
	private static final OutputHandler outputHandler = new OutputHandler();
	
	public static void main(String[] args) throws XInputNotLoadedException
	{
		System.out.println(XInputDevice14.isAvailable());
		System.out.println("Hello world!");
		XInputDevice14[] devices = XInputDevice14.getAllDevices();
		for (XInputDevice14 device : devices)
		{
			if (device.isConnected())
			{
				System.out.println("Controller detected!");
				handleDevice(device);
			}
		}
		System.out.println("Bye!");
	}
	
	private static void handleDevice(XInputDevice14 device)
	{
		Controller controller = new Controller();
		
		int leftAnalogIdle = 1000;
		int rightAnalogIdle = 1000;
		Point originalMouse = MouseInfo.getPointerInfo().getLocation();
		double mouseX = originalMouse.x;
		double mouseY = originalMouse.y;
		
		while (device.poll())
		{
			XInputComponents components = device.getComponents();
			controller.update(components);
			if (controller.isJustPressed(Button.A))
			{
				outputHandler.snowball();
			}
			
			if (controller.isJustPressed(Button.B))
			{
				outputHandler.snowstorm();
			}
			
			if (controller.isJustPressed(Button.X))
			{
				outputHandler.chomp();
			}
			
			if (controller.isJustPressed(Button.Y))
			{
				outputHandler.tinySnowballs();
			}
			
			if (controller.isJustPressed(Button.RT))
			{
				outputHandler.flash();
			}
			
			if (controller.isJustPressed(Button.LT))
			{
				outputHandler.smite();
			}
			
			if (controller.isJustPressed(Button.RB))
			{
				outputHandler.autoAttack();
			}
			
			if (controller.isJustPressed(Button.LB))
			{
				outputHandler.stop();
			}
			
			if (controller.isJustPressed(Button.DL))
			{
				outputHandler.leftClick();
			}
			
			if (controller.isJustPressed(Button.GD))
			{
				outputHandler.lockCamera();
			}
			
			if (controller.isJustPressed(Button.ST))
			{
				outputHandler.recall();
			}
			
			if (controller.isJustPressed(Button.UP))
			{
				outputHandler.slot1();
			}
			
			if (controller.isJustPressed(Button.DW))
			{
				outputHandler.slot2();
			}
			
			if (controller.isJustPressed(Button.RG))
			{
				outputHandler.openShop();
			}
			
			XInputAxes axes = components.getAxes();
			
			float leftAnalogMovement = Math.abs(axes.lx) + Math.abs(axes.ly);
			if (leftAnalogMovement < 0.1)
			{
				leftAnalogIdle++;
			}
			else
			{
				leftAnalogIdle = 0;
			}
			if (leftAnalogIdle < 200)
			{
				outputHandler.aim(axes.lx, -axes.ly);
			}
			boolean rightAnalogLocalIdle = Math.abs(axes.rx) < 0.06 && Math.abs(axes.ry) < 0.06;
			if (rightAnalogLocalIdle)
			{
				rightAnalogIdle++;
			}
			else
			{
				rightAnalogIdle = 0;
			}
			if (rightAnalogIdle < 200 && rightAnalogLocalIdle == false)
			{
				mouseX += axes.rx * 0.2;
				mouseY += -axes.ry * 0.2;
				outputHandler.moveMouse((int) Math.round(mouseX), (int) Math.round(mouseY));
			}
		}
	}
	
}

package net.fehleno.nunu;

public class NunuException extends RuntimeException
{
	public NunuException(String message)
	{
		super(message);
		
	}
	
	public NunuException(String message, Throwable cause)
	{
		super(message, cause);
		
	}
	
}

package net.fehleno.nunu;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;


public class OutputHandler
{
	private final Robot robot;
	
	public OutputHandler()
	{
		try
		{
			robot = new Robot();
		}
		catch (AWTException e)
		{
			throw new NunuException("Failed to construct output handler", e);
		}
	}
	
	public void snowball()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_W);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_W);
			System.out.println("Pressed W");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to turn on the snowball", e);
		}
	}
	
	public void chomp()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_Q);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_Q);
			System.out.println("Pressed Q");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to chomp", e);
		}
	}
	
	public void tinySnowballs()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_E);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_E);
			System.out.println("Pressed E");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to throw tiny snowballs", e);
		}
	}
	
	public void snowstorm()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_R);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_R);
			System.out.println("Pressed R");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to cast snowstorm", e);
		}
	}
	
	public void flash()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_F);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_F);
			System.out.println("Pressed F");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to flash", e);
		}
	}
	
	public void smite()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_D);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_D);
			System.out.println("Pressed D");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to smite", e);
		}
	}
	
	public void lockCamera()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_Y);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_Y);
			System.out.println("Pressed Y");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to lock camera", e);
		}
	}
	
	public void recall()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_B);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_B);
			System.out.println("Pressed B");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to recall", e);
		}
	}
	
	public void slot1()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_1);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_1);
			System.out.println("Pressed 1");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed use item NUM 1", e);
		}
	}
	
	public void slot2()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_2);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_2);
			System.out.println("Pressed 2");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed use item NUM 2", e);
		}
	}
	
	public void openShop()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_P);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_P);
			System.out.println("Pressed P");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed open the shop", e);
		}
	}
	
	public void autoAttack()
	{
		try
		{
			robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
			Thread.sleep(50);
			robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
			System.out.println("Right Clicked");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to auto attack", e);
		}
	}
	
	public void stop()
	{
		try
		{
			robot.keyPress(KeyEvent.VK_S);
			Thread.sleep(50);
			robot.keyRelease(KeyEvent.VK_S);
			System.out.println("Pressed S");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to stop", e);
		}
	}
	
	public void leftClick()
	{
		try
		{
			robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
			Thread.sleep(50);
			robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
			System.out.println("Left Clicked");
		}
		catch (InterruptedException e)
		{
			throw new NunuException("Failed to left click", e);
		}
	}
	
	public void aim(float x, float y)
	{
		int mouseX = (int) ((1980 / 2) + (x * 200));
		int mouseY = (int) ((1080 / 2) + (y * 300));
		robot.mouseMove(mouseX, mouseY);
	}
	
	public void moveMouse(int mouseX, int mouseY)
	{
		robot.mouseMove(mouseX, mouseY);
	}
	
}
